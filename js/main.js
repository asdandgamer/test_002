/* file:    main.js
    author: asdanju
    mail:   asdandgamer@gmail.com
    descrioptioin: Site funcs.
*/
$(document).ready(function () {
    Resize();
    $('#find_in').on('keypress',function(e) {
        if(e.which == 13) Find();
    });
});
$(window).resize(Resize);

function Resize() {
    var width = $('.item').width()-2.5*16;
    $('.item_image').css('width', width);
}
// On click [Find] button
function Find() {
    find_str = $('#find_in').val();
    window.location.replace("/item/find/"+find_str+'/');
}
function ShowCats() {
    $('.dropdown-menu').toggle();
}
function OpenItem(id) {
    window.location.replace('/item/id/'+id+'/');
}