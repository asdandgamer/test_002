/* file:    editor.js
    author: asdanju
    mail:   asdandgamer@gmail.com
    descrioptioin: Editor funcs;
*/
var categoryIsEmpty=false;
var imgLink = '/img/empty.jpg';
$(document).ready(function () {
    // $('.edit_btn').click(function () {

    // });
});
$(window).resize(ResizeEditor);
function OpenWnd(wnd)    { $(wnd).show(); }
function CloseWnd(wnd)   { $(wnd).hide(); }
function ClrImgLink() { imgLink = '/img/empty.jpg'; }
function ResizeEditor() {
    if($(window).height() < $('.editor').height()+40) {
        $('.editor').css('margin', 'auto');
        $('.editor_holder').css('position', 'absolute');
        $('.editor_holder').css('height', 'unset');
    } else {
        $('.editor').css('margin', '40px auto auto');
        $('.editor_holder').css('position', 'fixed');
        $('.editor_holder').css('height', '100%');
    }
}
function EmptyEditorFields() {
    $('#ititle').val('');
    $('#isku').val('');
    $('#iprice').val('');
    $('#idscr').val('');
    $('#icrat').text('');
}
function OnError(xhr, estr=null) {
    console.error(xhr);
    // alert('Failure: ' + xhr.responseText);
    $('#alert').html('Failure: ' + xhr.responseText);
    $('#alert').show(); // fadeIn
    $('#alert').alert('dispose');
}
function Delete(id) {
    data = {
        controller: 'item',
        action: 'delete',
        data: {
            id: id
        }
    }
    $.post("/", data)
    .done(function(data) {
        window.location.replace("/item/");
    })
    .fail(function(xhr){ OnError(xhr); });
}
function Edit(id) { // on editor open
    $.ajax({
        url: '/item/getid/'+id,
        method: 'GET',
        dataType: 'json',
        success: function(data) {
            //alert(JSON.stringify(data));
            $('#iid').text(data.id);
            $('#ititle').val(data.title);
            $('#isku').val(data.sku);
            $('#iprice').val(data.price);
            $('#idscr').val(data.description);
            $('#icrat').text(data.added);
            $('#load_img').show();
            $('#load_img').attr('src', data.image);
            imgLink = data.image;
            FillCats(data.category_id);
        },
        error: function(xhr){ OnError(xhr); }
    });
    OpenWnd('.editor_holder');
    ResizeEditor();
}
function Add() {
    ClrImgLink();
    FillCats();
    OpenWnd('.editor_holder');
    ResizeEditor();
    $('#icrat').parent().hide();
    $('#btn_del_editor').hide();
}
function Send(id=0) {
    if(!ValidateFields()) { alert('Some of fields are empty!'); return; }
    var values = {
        id: id,
        title: $('#ititle').val(),
        sku: $('#isku').val(),
        price: $('#iprice').val(),
        descr: $('#idscr').val(),
        image: imgLink,
        category_id: $('#categories').find(":selected").val()
    };
    var data = {
        controller: 'item',
        action: 'set',
        data: values
    };
    $.ajax({
        url: '/',
        method: 'POST',
        data: data,
        dataType: 'json',
        success: function(data) {
            // alert(JSON.stringify(data));
            if(data.status) {
                EmptyEditorFields();
                ClrImgLink();
                window.location.replace("/item/");
            } else
                OnError(null, data.msg);
        },
        error: function (xhr) { OnError(xhr); }
    });
    CloseWnd('.editor_holder');
}
function UploadImg() {
    var fdata = new FormData();
    fdata.append('file', $('#img_in')[0].files[0]);
    $.ajax({
		url: '/',
		data: fdata,
		processData: false,
		contentType: false,
        type: 'POST',
        dataType: 'json',
		// this part is progress bar
		xhr: function () {
			var xhr = new window.XMLHttpRequest();
			xhr.upload.addEventListener("progress", function (evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					percentComplete = parseInt(percentComplete * 100);
					$('#upimg_progress').css('width', percentComplete + '%');
				}
			}, false);
			return xhr;
		},
		success: function (data) {
            imgLink = data.link;
            $('#load_img').attr('src', data.link);
            $('#load_img').show();
            ResizeEditor();
        },
        error: function(xhr){ OnError(xhr); }
	});
}
function FillCats(catid=null) { // fill categories dropdown in editor
    if(categoryIsEmpty) return;
    // --------------------------------------------------------------
    $.getJSON('/item/getcats/')
    .done(function(data) {
        // alert(JSON.stringify(data) + data.length);
        $('#categories').empty();
		for (var i = 0; i < data.length; i++) {
			var opt = document.createElement('option');
			opt.value = (Object.values(data))[i][0];
			opt.innerHTML = (Object.values(data))[i][1];
			$('#categories').append(opt);
        }
        if(catid) $('#categories').val(catid);
        categoryIsEmpty = false;
    })
    .fail(function(xhr){ OnError(xhr); });
}
function ValidateFields() {
    if( ($('#ititle').val()==='') &&
        (typeof $('#isku').val()!=='number') &&
        (typeof $('#iprice').val()!=='number') &&
        ($('#idscr').val()==='') )
        return false;
    else return true;
}