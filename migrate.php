<?php
/*	FILE:	migrate.php
*	author:	asdandgamer
*	e-mail:	asdandgamer@gmail.com
* -------------------------------- *
*   date:   17.08.2019
* -------------------------------- *
* TO DO: 
*/
ini_set("display_errors", true);
require_once 'application/core/DB.php';

$db = DB::Instance();
if(!$db->isConnected())
    die($db->getError());
// create database --------------------------------------------------
// GRANT ALL ON pulmrocket_Test.* to andrew IDENTIFIED BY 'mode3DLL';
// if($db->Exec('CREATE DATABASE pulmrocket_Test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci', true))
//     echo 'database \'Pulmrocket_Test\' succelfully created';
// else
//     echo $db->getError(true);
// create user ------------------------------------------------------
/*if($db->Exec("CREATE TABLE user (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT, 
    login VARCHAR(16) NOT NULL, 
    pass VARCHAR(32) NOT NULL,
    mail varchar(255) NOT NULL,
    level enum('unchecked', 'blocked', 'closed', 'user', 'moder', 'admin') DEFAULT 'unchecked',
    avatar TEXT,
    register TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    UNIQUE(login)
    )", true))
    //------------------------------------
    echo 'TABLE \'user\' CREATED : ok <br>';
else
    echo $db->getError(true);*/
// create category --------------------------------------------------
//CREATE TABLE category(id INT UNSIGNED NOT NULL AUTO_INCREMENT, name VARCHAR(255) NOT NULL, PRIMARY KEY(id));
if($db->Exec("CREATE TABLE category(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
    )", true))
    //------------------------------------
    echo 'TABLE \'category\' CREATED : ok <br>';
else
    echo $db->getError(true);
// create item ------------------------------------------------------
if($db->Exec("CREATE TABLE item (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT, 
    title TEXT NOT NULL, 
    description TEXT NOT NULL,
    image TEXT,
    price FLOAT UNSIGNED,
    SKU INT UNSIGNED,
    added TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    autor_id INT UNSIGNED NOT NULL,
    category INT UNSIGNED NOT NULL,
    PRIMARY KEY(id)
    )", true))
    //------------------------------------
    echo 'TABLE \'user\' CREATED : ok <br>';
else
    echo $db->getError(true);
// add dependencies -------------------------------------------------
if($db->Exec("ALTER TABLE item ADD FOREIGN KEY (category) REFERENCES category(id)", true))
    echo 'ALTER TABLE \'item\' : ok <br>';
else
    echo $db->getError(true);

// $header = array('Field', 'Type', 'Null', 'Key', 'Default', 'Extra');

// print(TableToHtml($db->Describe('user'), $header));
// print(TableToHtml($db->Describe('game'), $header));
// print(TableToHtml($db->Describe('disk'), $header));
?>