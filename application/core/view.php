<?php /*
    file:   application/core/view.php
    autor:  asdanju
    mail:       asdandgamer@gmail.com
    created:    28.07.2019
    used sources: {
        https://habr.com/ru/post/150267/
    }
*/

class View
{
	//public $template_view; // здесь можно указать общий вид по умолчанию.
	
	function generate($content_view, $template_view, $data = null) {
		/*
		if(is_array($data)) {
			// преобразуем элементы массива в переменные
			extract($data);
		}
		*/
		if(!empty($template_view)) include 'application/views/'.$template_view;
	}
}