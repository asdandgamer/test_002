<?php
/*	FILE:	DB.php @ ENGINE
*	author:	asdandgamer
*	e-mail:	asdandgamer@gmail.com
* -------------------------------- *
*   date:   29.12.2018
*/
// ini_set("display_errors", true);
// require_once $_SERVER["DOCUMENT_ROOT"].'/ENGINE/Interfaces.php';

define("DB_INI", 'application/core/.db.ini'); // path to database connection settings ini file

define("MOD_AND", ' && ');
define("MOD_OR", ' || ');
define("MOD_NOT", ' != ');
define("MOD_MORE", ' > ');
define("MOD_LESS", ' < ');
define("MOD_EQ", " = ");
define("MOD_MOREEQ", " >= ");
define("MOD_LESSEQ", " <= ");

define("E001", 'E001: No settings file!'.DB_INI);
define("E002", 'E002: You\'re not connected!');
define("E003", 'E003: DB::Select() Condition array is given, but modificator is NULL.');
define("E004", 'E004: DB::Update() Condition array is given, but modificator is NULL.');

// define("DEBUG_", 0);

/* USAGE:
	: At start.
	$db = DB::Instance();
	if(!$db->isConnected()) die($db->getError(true));
	//----------------------------------
	: DESCRIBE :
		$db->Describe(<table_name>)
		print_r($db->Describe('user'));
	: SELECT :
		$db->Select(<table_name>, <fields>, <condition>)
*/

class DB // implements iDB
{
	private $servername;
	private $username;
	private $password;
	private $dbname;
	private $connection;
	//---------------------------------------------------------------
	private $isConnected;
	private $error;
	private $lastRequest;
	private $lastResult;

	public static function Instance($dbConnData=false) { // Singleton
        static $inst = null;
        if ($inst === null) $inst = new DB($dbConnData);
        return $inst;
    }
	private function __construct($dbConnData) {
		$this->isConnected = false;
		//-----------------------------------------------------------
		if(!$dbConnData) //get from file if params is empty
			if(file_exists(DB_INI))
				$dbConnData = parse_ini_file(DB_INI); // get connection data from ini file
			else
				return $this->SetError(E001);
		//-----------------------------------------------------------
		$this->servername = $dbConnData["DB_HOST"];
		$this->username = $dbConnData["DB_USER"];
		$this->password = $dbConnData["DB_PASS"];
		$this->dbname = $dbConnData["DB_NAME"];
		//-----------------------------------------------------------
		$this->Connect();
	}
	// STD ----------------------------------------------------------
	public function Describe($tableName) { // Returns Description of table
		$requestString = "DESCRIBE " . $tableName;
		if($this->Do($requestString) === false) return false;
		return $this->lastResult;
	}
	public function Select($tableName, $fields=NULL, $condition=NULL, $mod=NULL, $order=NULL, $like=null) { // Returns Selected table data by the values and selected columns
		if(is_array($fields))
			$fields = implode(", ", $fields);
		if(is_array($condition))
			if($mod !== NULL) $condition = implode($mod, $condition);
			else $condition = implode(', ', $condition); //return $this->SetError(E003);
		//---------------------------------------------------------------------
		if($fields != NULL)
			$requestString = "SELECT " . $fields . " FROM " . $tableName;
		else
			$requestString = "SELECT * FROM " . $tableName;
		//---------------------------------------------------------------------
		if($condition != NULL)
			$requestString = $requestString . " WHERE " . $condition;
		//---------------------------------------------------------------------
		if($order != NULL)
			$requestString = $requestString . " ORDER BY " . $order;
		//---------------------------------------------------------------------
		if($like != NULL)
			$requestString = $requestString . ' LIKE \'%' . $like . '%\'';
		//  RETURN  -----------------------------------------------------------
		if(!$this->Do($requestString)) return false;
		return $this->lastResult;
	}
	public function Insert($tableName, $fields, $values) { // Inserting data to table
		if(is_array($fields))
			$fields = implode(", ", $fields);
		//---------------------------------------------------------------------
		$requestString = "INSERT INTO " . $tableName . " (" . $fields . ") VALUES (";
		$vals = "";
		//---------------------------------------------------------------------
		if(is_array($values)) {
			for($i = 0; $i < count($values); $i++) {
				if(is_numeric($values[$i]))
					$vals = $vals . $values[$i];
				else
					$vals = $vals . '\'' . $values[$i] . '\'';
				if($i<count($values)-1)
					$vals = $vals . ', ';
			}
		} else {
			$vals = $values;
		}
		$requestString = $requestString . $vals . ")";
		//  RETURN  -----------------------------------------------------------
		return $this->Do($requestString, true);
	}
	public function Update($tableName, $fields, $values, $condition, $mod=NULL) {
		if(is_array($condition))
			if($mod !== NULL) $condition = implode($mod, $condition);
			else return $this->SetError(E004);
		$requestString = 'UPDATE ' . $tableName . ' SET ';
		$columns = "";
		//---------------------------------------------------------------------
		if(is_array($fields)) {
			for ($i=0; $i < count($fields); $i++) { 
				if(is_numeric($values[$i]))
					$columns = $columns . $fields[$i] . '=' . $values[$i];
				else
					$columns = $columns . $fields[$i] . '=\'' . $values[$i] . '\'';
				if($i<count($values)-1)
					$columns = $columns . ', ';
			}
		} else {
			if(is_numeric($values)){
				$columns = $fields . '=' . $values;
			} else{
				$columns = $fields . '=\'' . $values . '\'';
			}
		}
		//---------------------------------------------------------------------
		$requestString = $requestString . $columns . ' WHERE ' . $condition;
		//  RETURN  -----------------------------------------------------------
		return $this->Do($requestString, true);
	}
	public function Delete($tableName, $condition=NULL, $mod=NULL) { // Removing data from table. If no condition -> table will be empty
		if($condition === NULL)
			$requestString = "DELETE FROM " . $tableName;
		else {
			if(is_array($condition))
				if($mod !== NULL) $condition = implode($mod, $condition);
				else return $this->SetError(E003);
			//-----------------------------------------------------------------
			$requestString = "DELETE FROM " . $tableName . " WHERE " . $condition;
		}
		//  RETURN  -----------------------------------------------------------
		return $this->Do($requestString, true);
	}
	// MOD ----------------------------------------------------------
	public function InsertValues($tableName, $values) {
		return $this->Insert($tableName, $this->GetFieldsNames($tableName), $values);
	}
	// ADDITIONAL ---------------------------------------------------
	public function GetFieldsNames($tableName) { // Returns Fields names
		$fieldsNames = array();
		$tableDescription = $this->Describe($tableName);
		if($tableDescription == false) return false;
		for($i = 0; $i < count($tableDescription); $i++)
			array_push($fieldsNames, $tableDescription[$i][0]);
		return $fieldsNames;
	}
	public function Exec($request, $noResult=false) { // Exec MySQL command with results like SHOW TABLES;
		$res = $this->Do($request, $noResult);
		if($noResult) return $res;
		if($res != false) return $this->lastResult;
		else return false;
	}
	// PRIVATE ------------------------------------------------------
	private function Connect() { // Connecting to database
		$this->connection = new mysqli($this->servername, 
			$this->username, $this->password, $this->dbname);
		//-----------------------------------------------------------
		if ($this->connection->connect_error)
			$this->isConnected = $this->SetError($this->connection->connect_error);
		else 
			{ $this->error = NULL; $this->isConnected = true; }
	}
	private function Do($requestString, $noResult=false) {
		if(!$this->isConnected)
			return $this->SetError(E002);
		//-----------------------------------------------------------
		$this->lastRequest = $requestString;
		$this->lastResult = NULL;
        $result = $this->connection->query($requestString);
		//-----------------------------------------------------------
		if($this->connection->error)
			return $this->SetError($this->connection->error);
		else 
			$this->error = NULL;
		//-----------------------------------------------------------
		if(!$noResult) {
			$arr = array();
            if ($result->num_rows == 1)
                if($result->field_count == 1)
                    $arr = $result->fetch_array(MYSQLI_NUM)[0];
                else
                    $arr = $result->fetch_array(MYSQLI_NUM);
			else
				while($row = $result->fetch_array(MYSQLI_NUM))
                    array_push($arr, $row);
			$this->lastResult=$arr;
		}
		//  RETURN  -------------------------------------------------
		return true;
	}
	// Get ----------------------------------------------------------
	public function isConnected() {
		return $this->isConnected;
	}
	public function GetLastRequest() {
		return $this->lastRequest;
	}
	public function getError($fullErrorText=false) {
		$errorSTR = '</br><b>MySQL error message:</b> "' . $this->error 
				. '"</br><b>Last request:</b> "' . $this->lastRequest . '"</br>';
		if($fullErrorText) return $errorSTR;
		else return $this->error;
    }
    public function GetLastIndex($table, $field) {
        return $this->Select($table, 'MAX('.$field.')')[0];
    }
	// Set ----------------------------------------------------------
	private function SetError($err_str=NULL) {
		if($err_str == NULL)
			if($this->connection->error) {
				$this->error = $this->connection->error;
				return false;
			} else
				return true;
		else {
			$this->error = $err_str;
			return false;
		}
	}
	public function SetCharset($charSet) {
		$this->connection->set_charset($charSet);
	}
}
