<?php /*
    file:       application/core/route.php
    autor:      asdanju
    mail:       asdandgamer@gmail.com
    created:    28.07.2019
    used sources: {
        https://habr.com/ru/post/150267/
    }
*/

class Route
{
	static function start() {
		switch ($_SERVER["REQUEST_METHOD"]) { // detect Method
			case 'GET': self::get(); break;
			case 'POST': self::post(); break;
			default: self::ErrorPage404("Undefined request method");break;
		}
	}
	//-------------------------------------------------------------------------
	function get($controller_name='Main', $action='action_index') { // Method GET
		//$controller_name = 'Main'; $action = 'action_index'; // defaults
		if($controller_name!='Menu') $routes = explode('/', $_SERVER['REQUEST_URI']);
		//---------------------------------------------------------------------
		if(!empty($routes[1])) $controller_name = $routes[1];	// get controller name
		if(!empty($routes[2])) $action = 'action_'.$routes[2];		// get action name
		!empty($routes[3]) ? $params = $routes[3] : $params=null;		// get params
		!empty($routes[4]) ? $params = array($routes[3], $routes[4]) : null;
		self::Exec($controller_name, $action, $params);
	}
	function post() {
		if(empty($_POST) && isset($_FILES)) {
			self::Exec('item', 'action_upimg', $_FILES);
			return;
		}
		if(!isset($_POST)) self::ErrorPage500('Nothing to post');
		!empty($_POST['controller']) ? $controller_name = $_POST['controller'] : self::ErrorPage404(__LINE__.json_encode($_POST));
		!empty($_POST['action']) ? $action = $_POST['action'] : self::ErrorPage404(__LINE__);
		!empty($_POST['data']) ? $data = $_POST['data'] : self::ErrorPage404(__LINE__);
		self::Exec($controller_name, 'action_'.$action, $data);
	}
	function Exec($controller_name, $action, $params=null) {
		// add prefixes -------------------------------------------------------
		$model_name = 'Model_'.$controller_name;
		$controller_name = 'Controller_'.$controller_name;
		// include model file (model file may not exist) ----------------------
		$model_file = "application/models/".strtolower($model_name).'.php';
		if(file_exists($model_file)) include_once $model_file; 
		// include controller file
		$controller_file = "application/controllers/".strtolower($controller_name).'.php';
		if(file_exists($controller_file)) include_once $controller_file;
		else self::ErrorPage404(__LINE__.'<br>No controller file!');
		// create controller
		$controller = new $controller_name;
		if(method_exists($controller, $action) && empty($params))
			$controller->$action();
		else if(method_exists($controller, $action) && !empty($params))
			$controller->$action($params);
		else 
			self::ErrorPage404('unidefined action: '.$controller_name.'->'.$action);
	}
	//-------------------------------------------------------------------------
	function ErrorPage404($msg) {
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:http://'.$_SERVER['HTTP_HOST'].'/404');
		$estr = '<h1 align="center">404</h1>'.
		'<div style="border-left: 3px solid red; padding-left: 3px;'.
			' background-color: #eee; font-family: Verdana">'.
		'<div>'.__FILE__.'<br>'.$msg.'</div></div>';
		die($estr);
	}
	function ErrorPage500($msg) {
		header('HTTP/1.1 500 Not Found');
		header("Status: 500 Not Found");
		//header('Location:http://'.$_SERVER['HTTP_HOST'].'/404');
		die(__FILE__.$msg);
		
	}
}