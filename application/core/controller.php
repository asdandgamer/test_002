<?php /*
    file:   application/core/controller.php
    autor:  asdanju
    mail:       asdandgamer@gmail.com
    created:    28.07.2019
    used sources: {
        https://habr.com/ru/post/150267/
    }
*/

class Controller
{
	public $model;
	public $view;
	
	function __construct() {
		$this->view = new View();
	}
	
	function action_index() { }
}