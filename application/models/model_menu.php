<?php
require_once 'application/core/DB.php';
class Model_Menu extends Model
{
	private $db;
	//-------------------------------------------------------------------------
	function __construct() { 
		$this->db = DB::Instance();
		if(!$this->db->isConnected()) die($this->db->getError(true));
	}
	public function get_data() {
		return $this->db->Select('category', 'name');
    }
}