<?php /* 
    


    TASK: Для кожного продукту потрібно показати його фото, назву, вартість, а також посилання для переходу для детального перегляду.
*/
ini_set("display_errors", true);

require_once 'application/core/DB.php';

class Model_Item extends Model
{
	private $db;
	//-------------------------------------------------------------------------
	function __construct() { 
		$this->db = DB::Instance();
		if(!$this->db->isConnected()) die($this->db->getError(true));
	}
	public function get_data($id=null, $sort_by=null) {
		// echo 'asfaf'. json_encode($this->db->Exec("SHOW TABLES"));
		if($id!=null)
			return $this->FormatArrFromSQL($this->db->Select('item', null, 'id='.$id))[0];
		else if ($sort_by!=null)
			return $this->FormatArrFromSQL($this->db->Select('item', null, null, null, $sort_by));
		else
			return $this->FormatArrFromSQL($this->db->Select('item'));
	}
	public function get_category($category) {
		$catId = $this->db->Select('category', 'id', 'name=\''.$category.'\'');
		return $this->FormatArrFromSQL($this->db->Select('item', null, 'category=\''.$catId.'\''));
	}
	public function get_find($find_str) {
		$result = $this->db->Select('item', null, 'title', null, null, $find_str);
		if($result === null || empty($result)) $result = $this->db->Select('item', null, 'description', null, null, $find_str);
		if($result === null || empty($result)) return null;
		if($result === false) return false;
		else return $this->FormatArrFromSQL($result);
	}
	public function get_categories() {
		return $this->db->Select('category');
	}
	public function set_data($data) {
		if($data['id']==0) { 	// On adding new
			$data['id']=$this->db->GetLastIndex('item', 'id')+1; 
			$data['added']=date('Y-m-d H:i:s');
			return $this->db->InsertValues('item', $this->FormatArrToSQL($data));
		} else { 				// On Updating existing item
			// $fileds = $this->db->GetFieldsNames();
			// $data = $this->FormatArrToSQL($data);
			$fields = array('title', 'description', 'image', 'price', 'SKU', 'category');
			return $this->db->Update('item', $fields, 
				$this->FormatArrToSQL($data, true), 
				'id='.$data['id']);
		}		
	}
	public function delete_item($id) {
		return $this->db->Delete('item', 'id='.$id);
	}
	public function GetError() {
		return $this->db->getError(true);
	}
	//-------------------------------------------------------------------------
	private function FormatArrFromSQL($arr) {
		$fin = array();
		if(is_array($arr[0]))
			foreach($arr as $row) {
				$tmp = array(
					'id' => $row[0],
					'title' => $row[1],
					'description' => $row[2],
					'image' => $row[3],
					'price' => $row[4],
					'sku' => $row[5],
					'added' => $row[6],
					'category_id' => $row[8],
					'link' => '/item/id/'.$row[0]);
				array_push($fin, $tmp);
			}
		else 
			array_push($fin, array(
				'id' => $arr[0],
				'title' => $arr[1],
				'description' => $arr[2],
				'image' => $arr[3],
				'price' => $arr[4],
				'sku' => $arr[5],
				'added' => $arr[6],
				'category_id' => $arr[8],
				'link' => '/item/id/'.$arr[0]));
		return $fin;
	}
	private function FormatArrToSQL($arr, $update=false) {
		if($update)
			return array(
				$arr['title'],
				$arr['descr'],
				$arr['image'],
				$arr['price'],
				$arr['sku'],
				$arr['category_id']);
		return array(
			$arr['id'],
			$arr['title'],
			$arr['descr'],
			$arr['image'],
			$arr['price'],
			$arr['sku'],
			$arr['added'],
			1, 
			$arr['category_id']);
	}
}
// | Field       | Type             | Null | Key | Default           | Extra          |
// +-------------+------------------+------+-----+-------------------+----------------+
// | id          | int(10) unsigned | NO   | PRI | NULL              | auto_increment |
// | title       | text             | NO   |     | NULL              |                |
// | description | text             | NO   |     | NULL              |                |
// | image       | text             | YES  |     | NULL              |                |
// | price       | float unsigned   | YES  |     | NULL              |                |
// | SKU         | int(10) unsigned | YES  |     | NULL              |                |
// | added       | timestamp        | NO   |     | CURRENT_TIMESTAMP |                |
// | autor_id    | int(10) unsigned | NO   |     | NULL              |                |
// | category    | int(10) unsigned | NO   | MUL | NULL              |                |