<?php /*
    file:       application/bootstrap.php
    autor:      asdanju
    mail:       asdandgamer@gmail.com
    created:    28.07.2019
    used sources: {
        https://habr.com/ru/post/150267/
    }
*/

require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/route.php';

Route::start(); // start router

?>