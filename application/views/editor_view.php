<div class="editor_holder">
  <div class="editor shadow-sm mb-5 bg-white rounded">
    <div class="wnd_top card-header mb-2 bg-dark text-white">
        ID: <strong id="iid">New</strong>
        <input type="button" class="btn btn-outline-danger btn_x btn-sm" value="&times;" onclick="CloseWnd('.editor_holder')">
    </div>
    <div class="wnd_content">
      <!-- title -->
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-default">Title</span>
        </div>
        <input id="ititle" type="text" class="form-control" aria-label="Title" aria-describedby="inputGroup-sizing-default">
      </div>
      <!-- SKU -->
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroup-sizing-default">SKU</span>
        </div> 
        <input id="isku" type="text" class="form-control" aria-label="SKU" aria-describedby="inputGroup-sizing-default">
      </div>
      <!-- Price  -->
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text">Price</span>
        </div>
        <input type="text" id="iprice" class="form-control" aria-label="Amount (to the nearest dollar)">
        <div class="input-group-append">
          <span class="input-group-text">₴ UAH</span>
        </div>
      </div>
      <!-- Category Select -->
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <label class="input-group-text" for="categories">Category</label>
        </div>
        <select class="custom-select" id="categories">
          <option selected>Choose...</option>
          <option value="1">One</option>
          <option value="2">Two</option>
          <option value="3">Three</option>
        </select>
      </div>
      <!-- Logo file upload -->
      <div class="input-group mb-1">
        <div class="custom-file">
          <input type="file" class="custom-file-input" id="img_in">
          <label class="custom-file-label" for="img_in" aria-describedby="img_in">Image file</label>
        </div>
        <div class="input-group-append">
          <span class="input-group-text btn-primary" id="inputGroupFileAddon02" onclick="UploadImg()">Upload</span>
        </div>
      </div>
      <div class="progress w100 mb-1">
        <div id="upimg_progress" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
      <div class="w100 mb-3">
        <img id="load_img" class="card-img-top" src="/img/empty.jpg" style="display: none">
      </div>
      <!-- Decription  -->
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">Decription</span>
        </div>
        <textarea id="idscr" class="form-control" aria-label="With textarea"></textarea>
      </div>
      <!-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->
      <label style="margin: .25rem 0 0 0;">Created at: <strong id="icrat"></strong></label>
    </div>
    <div class="dropdown-divider"></div>
    <div class="wnd_bottom" style="padding: 0 8px;">
      <input type="button" class="btn btn-outline-success" value="Save" onclick="Send(<?php if(isset($data['id'])) echo $data['id']; ?>)">
      <input type="button" class="btn btn-outline-warning" value="Close" onclick="CloseWnd('.editor_holder')">
      <input type="button" class="btn btn-outline-danger" value="Delete" onclick="Delete(<?php if(isset($data['id'])) echo $data['id']; ?>)" id="btn_del_editor">
    </div>
  </div>
</div>
<link rel="stylesheet" href="/css/editor.css">
<script src="/js/editor.js"></script>