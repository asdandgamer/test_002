<?php 
  define('INCLUDE_EDITOR', true);
  function print_row($row) {
    echo '<div class="item card border-light" onclick="OpenItem(\''.$row['id'].'\')">';
      echo '<div class="card-header"><h5>'.$row['title'].'</h5></div>';
      echo '<div class="card-body">';
        echo '<div><img class="item_image" src="'.$row['image'].'"></div>';
        echo '<div>'.$row['sku'].'</div>';
      echo '</div>';
      echo '<div class="card-footer">'.$row['price'].'</div>';
    echo '</div>';
  }
?>
<div class="content">
  <div class="manage">
    <div>
      <input type="button" value="Add new" class="btn btn-outline-primary w100" onclick="Add()">
    </div>
    <!-- Find input -->
    <div>
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search" aria-label="Recipient's username" aria-describedby="button-addon2" id="find_in">
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button" id="button-addon2" onclick="Find()">Find</button>
        </div>
      </div>
    </div>
  </div>
  <div class="sorting">
    <div class="sort_by">
      Sort by:
      <a class="btn btn-outline-info btn-sm" href="/item/order/title/">Title↑</a>
      <a class="btn btn-outline-info btn-sm" href="/item/order/title/down/">Title↓</a>
      <a class="btn btn-outline-info btn-sm" href="/item/order/price/">Price↑</a>
      <a class="btn btn-outline-info btn-sm" href="/item/order/price/down/">Price↓</a>
      <a class="btn btn-outline-info btn-sm" href="/item/order/added/up/">Added↑</a>
      <a class="btn btn-outline-info btn-sm" href="/item/order/added/down/">Added↓</a>
    </div>
  </div>
  <div class="items_container">
    <?php if (!empty($data['data']) && is_array($data['data']))
              foreach($data['data'] as $row)
                  print_row($row); ?>
  </div>
</div>
