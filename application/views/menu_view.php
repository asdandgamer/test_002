<div class="main_menu ">
  <div><strong>MENU</strong></div>
  <div><a class="btn btn-outline-secondary"  href="/">Main</a></div>
  <!-- Split dropright button -->
  <div class="btn-group dropright" style="display: flex;">
    <a class="btn btn-outline-secondary" href="/item/">Items</a>
    <button onclick="ShowCats()" type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <span class="sr-only">Toggle Dropright</span>
    </button>
    <div class="dropdown-menu">
      <?php foreach($data as $row)
              echo '<a class="btn btn-outline-secondary" href="/item/category/'.$row[0].'">'.ucfirst($row[0]).'</a>'; ?>
    </div>
  </div>
</div>