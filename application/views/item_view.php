<?php define('INCLUDE_EDITOR', true); 
$data = $data['data'];
?>
<div class="content">
  <div class="manage">
    <div>
      <input type="button" value="Edit" class="btn btn-outline-primary w100" onclick="Edit(<?php echo $data['id']; ?>)">
    </div>
    <!-- Find input -->
    <div>
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search" aria-label="Recipient's username" aria-describedby="button-addon2" id="find_in">
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button" id="button-addon2" onclick="Find()">Find</button>
        </div>
      </div>
    </div>
  </div>
  <div class="item_m">
    <div class="item_img">
      <img src="<?php echo $data['image']; ?>" class="card-img-top" style="" alt="...">
    </div>
    <div class="card border-light">
      <div class="card-header">
        <h5><?php echo $data['title'];?></h5>
      </div>
      <div class="card-body">
        <?php
          echo '<div>SKU: '.$data['sku'].'</div>';
          echo '<div>Price: '.$data['price'].'</div>'; 
        ?>
      </div>
      <div class="card-footer"><?php echo date("d.m.Y H:i:s", strtotime($data['added']));?></div>
    </div>
  </div>
  <div class="item_c">
    <div class="card border-light">
      <div class="card-header"><h5>Description:</h5></div>
      <div class="card-body">
        <p class="card-text"><?php echo $data['description']; ?></p>
      </div>
    </div>
  </div>
</div>