<?php 
  function print_row($row) {
    echo '<div class="item card border-light" onclick="OpenItem(\''.$row['id'].'\')">';
      echo '<div class="card-header"><h5>'.$row['title'].'</h5></div>';
      echo '<div class="card-body">';
        echo '<div><img class="item_image" src="'.$row['image'].'"></div>';
        echo '<div>'.$row['sku'].'</div>';
      echo '</div>';
      echo '<div class="card-footer">'.$row['price'].'</div>';
    echo '</div>';
  }
?>
<div class="content">
<div class="manage">
    <div></div>
    <!-- Find input -->
    <div style="">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search" aria-label="Recipient's username" aria-describedby="button-addon2" id="find_in">
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button" id="button-addon2" onclick="Find()">Find</button>
        </div>
      </div>
    </div>
  </div>
  <div>
    <h1>Search results</h1>
  </div>
  <div class="items_container">
    <?php if (!empty($data['data']) && is_array($data['data']))
              foreach($data['data'] as $row)
                  print_row($row);
        else 
            echo 'Nothing to show'; ?>
  </div>
</div>