<?php

class Controller_Item extends Controller
{
	function __construct() {
		$this->model = new Model_Item();
		$this->view = new View();
	}
	// ------------------------------------------------------------------------
	function action_index() { // show all items
        $data = array( 'PageTitle'=>'Product list', 
                        'data'=> $this->model->get_data());
		$this->view->generate('items_view.php', 'template_view.php', $data);
    }
    function action_category($category) {
        $data = array(  'PageTitle'=>'Product list', 
                        'data'=>$this->model->get_category($category));
        $this->view->generate('items_view.php', 'template_view.php', $data);
    }
    function action_find($find_str) {
        $find = $this->model->get_find(urldecode($find_str));
        if($find===false) die($this->Status(false, $this->model->GetError()));
        $data = array(  'PageTitle'=>'Product search', 'data'=> $find);
        $this->view->generate('search_view.php', 'template_view.php', $data);
    }
    function action_order($sort_by) {
        if(is_array($sort_by) && count($sort_by)==2)
            switch ($sort_by[1]) {
                case 'up': $sort_by=$sort_by[0].' ASC'; break;
                case 'down': $sort_by=$sort_by[0].' DESC'; break;
                default: die($this->Status(false, 'Unknown Sorting param!')); break;
            }
        $list=$this->model->get_data(NULL, $sort_by);
        if($list === false) die($this->Status(false, $this->model->GetError()));
        $data = array(  'PageTitle'=>'Product list', 
                        'data'=> $list);
        $this->view->generate('items_view.php', 'template_view.php', $data);
    }
    function action_getid($id) { // all item info by id for editor
        echo json_encode($this->model->get_data($id));
    }
    function action_id($id) { // get all item info by id
        $itemData = $this->model->get_data($id);
        $data = array( 'PageTitle'=>$itemData['title'], 
                        'data'=> $itemData);		
		$this->view->generate('item_view.php', 'template_view.php', $data);
    }
    function action_delete($data) { // delete item
        if($this->model->delete_item($data['id'])) echo $this->Status();
        else die($this->Status(false, $this->model->GetError()));
    }
    function action_upimg($data) {
        if ($_FILES) {
            $name = $_FILES['file']['name'];
            $path = $_SERVER["DOCUMENT_ROOT"] . '/img/' . $name;
            if(move_uploaded_file($_FILES['file']['tmp_name'], $path)) 
                echo json_encode(array('Status'=>true,
                    'link'=>'/img/'.$name));
            else
                die($this->Status(false, "Error while storing file"));
        } else die($this->Status(false, "No uploaded file"));
    }
    function action_set($data) { // save item to database or update
        if($this->model->set_data($data))
            echo $this->Status();
        else
            die($this->Status(false, $this->model->GetError()));
    }
    function action_getcats() { // get categories
        echo json_encode($this->model->get_categories());
    }
    private function Status($status=true, $msg=null) { // show status
        if($status==true)
            return json_encode(array('status'=>true));
        else
            return json_encode(array('status'=>true, 'msg'=>$msg));
    }
}