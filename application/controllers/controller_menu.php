<?php 
class Controller_Menu extends Controller
{
	function __construct() {
		$this->model = new Model_Menu();
		$this->view = new View();
	}
	// ------------------------------------------------------------------------
	public function draw() { // show all items
		$this->view->generate(null, 'menu_view.php', $this->model->get_data());
    }
}